use serenity::{
    async_trait,
    model::{channel::Message, gateway::Ready},
    prelude::*,
};
use serenity::all::*;
use dotenv::dotenv;
use std::env;
use actix_cors::Cors;
use actix_web::{web::{self, Data}, App, HttpResponse, HttpServer, Responder};
use tokio::sync::mpsc::{Receiver, Sender, channel};
use tokio::sync::Mutex;
use std::sync::Arc;


struct Handler {
    rx: Arc<Mutex<Receiver<usize>>>,
}



#[async_trait]
impl EventHandler for Handler {
    async fn message(&self, ctx: Context, msg: Message) {
        if msg.content == "!start" {
            let rx = Arc::clone(&self.rx);
            let mut rx = rx.lock().await;
            loop {
                tokio::select! {
                    Some(_received_msg) = rx.recv() => {
                        let guilds: Vec<GuildId> = ctx.cache.guilds();
                        let mut guild_id_opt: Option<GuildId> = None;
            
                        for guild_id in guilds {
                            if guild_id != 802988337962680370 { continue }
                            guild_id_opt = Some(guild_id);
            
                        }
            
                        if guild_id_opt.is_none() { return; }
            
                        let guild_id = guild_id_opt.unwrap();
                        let guild = ctx.cache.guild(guild_id).unwrap().clone();
                        let http = ctx.http.clone();
            
                        let members: Vec<Member> = guild.search_members(http.clone(), "moek915", Some(1)).await.unwrap();
        
                        match members.first(){
                            Some(m) => {
                                m.mention();
                                m.ban(&http, 0).await.unwrap();
                            }
                            None => {msg.channel_id.say(&http, "MOŁKA NIE MA!!!!!!!!!!!").await.unwrap();}
                        };
                    }
                }
            }
        }
    }

    async fn ready(&self, _: Context, ready: Ready) {
        println!("{} is connected!", ready.user.name);
    }
}


#[tokio::main]
async fn main() {
    dotenv().ok();
    let (tx, rx): (Sender<usize>, Receiver<usize>) = channel(100);
    let handler = Handler { rx: Arc::new(Mutex::new(rx))};

    tokio::spawn(async move {
        Server::run(tx).await;
    });

    
    let token = env::var("DISCORD_TOKEN").expect("Expected a token in the environment");
    let intents: GatewayIntents = GatewayIntents::non_privileged() | GatewayIntents::MESSAGE_CONTENT;
    let mut client = Client::builder(&token, intents)
        .event_handler(handler)
        .await
        .expect("Error creating client");

    if let Err(why) = client.start().await {
        println!("Client error: {:?}", why);
    }
}

const HOST: &str = "localhost";
const PORT: &str = "5000";

pub struct Server;

impl Server {
	async fn health(app_state: Data<State>) -> impl Responder { 
        let tx = app_state.tx.clone();
        tx.send(1).await.unwrap();
        HttpResponse::Ok() 
    }

	pub async fn run(tx: Sender<usize>) {
        let app_state = State::new(tx);
        let app_data = Data::new(app_state);

		HttpServer::new(move || {
			App::new()
				.wrap(
					Cors::default() // Użyj domyślnej polityki CORS
						.allow_any_origin() // Zezwól na żądania z dowolnego pochodzenia
						.allow_any_method() // Zezwól na wszystkie metody HTTP
						.allow_any_header(), // Zezwól na wszystkie nagłówki
				)
                .app_data(app_data.clone())
				.route("/ban", web::get().to(Self::health))
        })
		.bind(format!("{HOST}:{PORT}")) // Zmień port, jeśli potrzebujesz
		.expect(format!("Can't bind to port {PORT}").as_str())
		.run()
		.await
		.expect("Actix Web server failed");
	}
}


#[derive(Clone)]
struct State{
    tx: Sender<usize>
}

impl State {
    fn new(tx: Sender<usize>) -> Self {
        Self { tx }
    }
}
